#include "gem5_cosim.hh"

irequest ireq;
iresponse ires;
drequest dreq;

int init()
{
    int num_args = 12;

    char *args[] = {
        "gem5.opt",
        "configs/example/se.py",
        "-c",
        "riscv_hello",                  // Test bench binary here
        "--cpu-type=TimingSimpleCPU",
        "--caches",
        "--sys-clock=300MHz",
        "--bp-type=BiModeBP",
        "--l1i_size=8kB",
        "--mem-size=256MB",
        "--mem-type=DDR4_2400_8x8",
        "--cosim-mode"
    };

    init_gem5(num_args, args);
    hijack_dport(0);
    hijack_iport(0);

    return 0;
}

extern "C" int step();
int step()
{
    int return_state = step_gem5();

    irequest ireq = l1ic_req_get(1);
    drequest dres = l1dc_req_get(1);

    return 0;
}

extern "C" irequest get_ireq();
irequest get_ireq()
{
    return ireq;
}

extern "C" drequest get_dreq();
drequest get_dreq()
{
    return dreq;
}

extern "C" int set_ires(uint64_t, uint64_t, uint8_t[32], uint64_t);
int set_ires(uint64_t valid, uint64_t trans_id, uint8_t data[32], uint64_t error)
{
    ires.valid = valid;
    ires.trans_id = trans_id;
    for (int i = 0; i < 32; i++)
    {
        ires.data[i] = data[i];
    }
    ires.error = error;

    return 0;    
}

extern "C" int set_dres(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t);
int set_dres(uint64_t valid, uint64_t opcode, uint64_t trans_id, uint64_t vaddr, uint64_t paddr, uint64_t size, uint64_t data, uint64_t error)
{
    dreq = {valid, opcode, trans_id, vaddr, paddr, size, data, error};

    return 0;
}