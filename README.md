# Repo of Lab1 of CPU Course Fall 2021

## Directory Structure

```text
.
├─output        DC output netlist files
├─tools         Co-Sim Tools
└─test_bench    Test Bench commands and outputs
```
